/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.fileinputoutput;

import java.io.Serializable;

/**
 *
 * @author Pla
 */
public class Rectangle  implements Serializable{
    private int width;
    private int high;

    public Rectangle(int width, int high) {
        this.width = width;
        this.high = high;
    }

    @Override
    public String toString() {
        return "Rectangle{" + "width=" + width + ", high=" + high + '}';
    }
    

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHigh() {
        return high;
    }

    public void setHigh(int high) {
        this.high = high;
    }
}
